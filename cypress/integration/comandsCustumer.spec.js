///<reference types="cypress" />
describe("TiCKETBOX", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("Preencher os campos obrigatorios e verificar campos obrigatorios", ()=> {
        const customer = {
            firstName:"João",
            lastName:"Mendes",
            email:"joão.mendes@gmail.com"
        };
        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("button[type='reset']")
            .click();
            
            cy.get("@submitButton")
                .should("be.disabled"); 

    });
});
