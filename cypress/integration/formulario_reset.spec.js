// ///<reference types="cypress" />
// describe("TiCKETBOX", () => {
//     beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

//     it("fills all input text in fileds", () =>{
//     const firstName = "Raphael";
//     const lastName = "Mendes";
//     const fullname = `${firstName} ${lastName}`;
//     cy.get("#first-name").type(firstName);
//     cy.get("#last-name").type(lastName);
//     cy.get("#email").type("mendes.raphaelrm@gmail.com");
//     cy.get("#requests").type("beer");
//     cy.get("#ticket-quantity").select("2");
//     cy.get("#vip").check();
//     cy.get("#social-media").check();
//     cy.get("#friend").check();

//     cy.get(".agreement p").should(
//         "contain",
//         `I, ${fullname}, wish to buy 2 VIP tickets.`
//     );

//     cy.get("#agree").check();
//     cy.get("#signature").type(`${fullname}`);

//     cy.get("button[type='submit']")
//     .as("submitButton")
//     .should("not.be.disabled");

//     cy.get("button[type='reset']").click();

//     cy.get("@submitButton").should("be.disabled"); 

//     }); 
// });
